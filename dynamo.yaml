AWSTemplateFormatVersion: '2010-09-09'
Description: Creates a DynamoDb table, with defaults
Metadata: 
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: Basic configuration
        Parameters:
          - TableName
          - PrimaryKeyName
          - PrimaryKeyType

Parameters:
  TableName:
    Description: Name of the DynamoDb Table
    Type: String
    AllowedPattern: '[a-zA-Z0-9._-]*'
    MinLength: '1'
    MaxLength: '250'
    ConstraintDescription: must contain only alphanumberic characters or _, -, and . 

  PrimaryKeyName:
    Description: PrimaryKey Name (leave this as 'id', for xby2do demo)
    Type: String
    AllowedPattern: '[a-zA-Z0-9]*'
    MinLength: '1'
    MaxLength: '2048'
    Default: id
    ConstraintDescription: must contain only alphanumberic characters

  PrimaryKeyType:
    Description: PrimaryKey Type 'S' or'N' (leave this as 'S', for xby2do demo)
    Type: String
    Default: S
    AllowedPattern: '[S|N]'
    MinLength: '1'
    MaxLength: '1'
    ConstraintDescription: must be either S or N

Resources:
  DynamoDBTable:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
      - AttributeName: !Ref 'PrimaryKeyName'
        AttributeType: !Ref 'PrimaryKeyType'
      KeySchema:
      - AttributeName: !Ref 'PrimaryKeyName'
        KeyType: HASH
      # Billing can be PROVISIONED or PAY_PER_REQUEST
      BillingMode: PROVISIONED         
      ProvisionedThroughput: 
        ReadCapacityUnits: "5"
        WriteCapacityUnits: "5"

  LambdaRole:
    Description: Allows Lambda Execution and DynamoDB Access
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
            Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action: sts:AssumeRole
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess     

Outputs:
  DynamoDBTable:
    Value: !Ref 'DynamoDBTable'
    Description: Table name of the newly created DynamoDB table
    Export:
      Name: 
        Fn::Sub: "${AWS::StackName}-DynamoDBTable"    

  LambdaRoleArn:
    Value: !GetAtt LambdaRole.Arn
    Description: Arn of the newly created LambdaRole (Execute and Dynamo access)
    Export:
      Name: 
        Fn::Sub: "${AWS::StackName}-LambdaRoleArn"    
                